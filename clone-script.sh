#!/bin/bash

set -ax

# git config --global user.email $email

rm -rf .git
ls -lart

#dir_name=`basename $(pwd)`

if [ "$repo_name" = "" ]; then
echo "Enter a Repo name"
exit 1
fi

# email=`git config --list|grep email`
# if [ "$email" = "" ]; then
# echo "Could not find email, run 'git config --global user.mail <mail>'"
# invalid_credentials=1
# fi

if [ "$token" = "" ]; then
echo "Could not find token. Please enter your token"
invalid_credentials=1
fi

if [ ! -z $invalid_credentials ]; then
exit 1
fi

echo -n "Creating GitLab repository '$repo_name' ..."

RepoId=`curl -s --header "PRIVATE-TOKEN: $token" -XPOST "https://gitlab.com/api/v4/projects?name=$repo_name&visibility=private&namespace_id=$GITLAB_SUBGROUPID" | jq '.id'`

# echo " done."
curl --header "PRIVATE-TOKEN: $token" "https://gitlab.com/api/v4/projects/$RepoId" > out.json

RepoSSHURL=`cat out.json | jq -r '.ssh_url_to_repo' | cut -d'@' -f2 | sed 's/:/\//'`
RepoHttpUrl=`cat out.json | jq -r '.http_url_to_repo'`
rm -rf out.json
#curl -s http://54.84.203.229/input.html > input.json
chmod 777 update.sh
./update.sh
#rm -rf input.json
echo -n "Pushing local code to remote ..."
git init
# echo "gitlab-init-remote.sh" > .gitignore
echo ".gitignore" >> .gitignore
 
git config --global core.excudefiles ~/.gitignore_global
git add .
git status
cat .gitignore
git commit -m "first commit"
git checkout -b development
git remote add origin https://PRIVATE-TOKEN:$token@$RepoSSHURL
#git push -u origin master
git push -u origin development
echo " done."

echo ""
echo "The created repo is available at following link:"
echo "$RepoHttpUrl"
