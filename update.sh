#!/bin/bash
while IFS= read -r line; do
line1=`echo "$line" | grep ChangeMe`
if [ ! -z "$line1" ]
then
QURY=`echo "$line1" | awk -F "ChangeMe" '{print $2}' | cut -d"-" -f2 | cut -d">" -f1`
#ChangeMe=`cat input.json | grep "$QURY" | cut -d":" -f2 | tr -d "," | tr -d ' "'`
echo "${!QURY}" | sed 's/\//\\\//g' 1>var
ChangeMe=`cat var`
rm var
if [ ! -z $ChangeMe ]
then
line=`echo "$line" | sed 's/<ChangeMe-'$QURY'>/'$ChangeMe'/g'`
fi
fi
echo "$line" >> new_deployment.yml
done < sample.deployment.yml
mv new_deployment.yml sample.deployment.yml

# sed -i 's/<ChangeMe-app_name>/'$app_name'/g' sample.deployment.yml
# echo $app_url | sed 's/\//\\\//g' 1>var
# app_url=`cat var`
# rm var
# sed -i 's/<ChangeMe-app_url>/'$app_url'/g' sample.deployment.yml
# sed -i 's/<ChangeMe-namesapce>/ns-'$app_name'/g' sample.deployment.yml
